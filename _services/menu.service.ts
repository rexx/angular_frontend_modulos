import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Globals } from '../globals';
import { MenuItem } from '../_models/menu';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MenuService {
  private URL_API: string;
  private bandLoadMenu = new Subject();
  public constructor(
    private http: HttpClient,
    private objGlobal: Globals
  ) {
    this.URL_API = this.objGlobal.BASE_URL + 'site/menu';
  }
  /**
   * Obtiene el menu del sistema para el usuario en session
   */
  public getMenu(idUsuario: number): Observable<MenuItem[]> {
    let params = new HttpParams();
    params = params.set('idUsuario', idUsuario + '');
    return this.http.get(
      (this.URL_API),
      { params }
    )
    .map(this.fnOnGetMenuSucccess.bind(this))
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  private fnOnGetMenuSucccess(response): MenuItem[] {
    return response.map((objMenuItem: MenuItem) => {
      return <MenuItem>objMenuItem;
    });
  }
  /**
   * Devuelve un observable para procesar la carga del menu
   * cuando la bandera sea activada por los subscriptores
   */
  public getBandLoadMenu(): Observable<any> {
    return this.bandLoadMenu.asObservable();
  }
  /**
   * Dispara la carga del menu
   */
  public fireLoadMenu(): void {
    this.bandLoadMenu.next();
  }
}
