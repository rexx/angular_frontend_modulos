import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoginService } from '../login/_services/login.service';
import { ToolbarService } from '../toolbar/_services/toolbar.service';

@Injectable()
export class GuardianGuard implements CanActivate {
  constructor(
    private objLoginService: LoginService,
    private objToolbarService: ToolbarService
  ) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      // Se aprovecha para verificar estado de la sesion y mostrar texto salir/login
      this.objToolbarService.fireBandIsLogin();
      return this.objLoginService.checkAccess(next.data.idPermiso);
  }
}
