import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Genero } from '../_models/genero';
import { Globals } from '../globals';

@Injectable()
export class GeneroService {
  private URL_API: string;
  public constructor(
    private http: HttpClient,
    private objGlobal: Globals
  ) {
    this.URL_API = this.objGlobal.BASE_URL + 'generos';
  }

  public getGeneros(): Observable<Genero[]> {
    const params = new HttpParams();
    return this.http.get(
      (this.URL_API),
      { params }
    )
    .map(this.fnOnGetGenerosSucccess.bind(this))
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  private fnOnGetGenerosSucccess(response): Genero[] {
    return response.map((objGenero: Genero) => {
      return <Genero>objGenero;
    });
  }
}
