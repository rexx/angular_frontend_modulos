import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class NavbarService {
  private bandToggleSideNavSource = new Subject();
  constructor() { }
  /**
   * Devuelve el observable para el tooglebotton
   */
  public getBandToggle(): Observable<any> {
    return this.bandToggleSideNavSource.asObservable();
  }
  /**
   * Dispara el evento de expansion/contraccion del sidenav
   */
  public fireToggleEvent(): void {
    this.bandToggleSideNavSource.next();
  }
}
