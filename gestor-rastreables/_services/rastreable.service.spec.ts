import { TestBed, inject } from '@angular/core/testing';

import { RastreableService } from './rastreable.service';

describe('RastreableService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RastreableService]
    });
  });

  it('should be created', inject([RastreableService], (service: RastreableService) => {
    expect(service).toBeTruthy();
  }));
});
