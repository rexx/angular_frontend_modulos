import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import { Rastreable } from '../_models/rastreable';
import { Globals } from '../../globals';

@Injectable()
export class RastreableService {
  private URL_API: string;
  public constructor(
    private http: HttpClient,
    private objGlobal: Globals
  ) {
    this.URL_API = this.objGlobal.BASE_URL + 'rastreables';
  }
  /**
   * Busca Rastreables en base a un termino de busqueda
   * @param query Termino de busqueda a aplicar
   */
  public search(query: Observable<string>, idUsuarioContratante?: number): Observable<Rastreable[]> {
    return query
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => this.getRastreables(term, idUsuarioContratante));
  }
  /**
   * Busca Rastreables disponibles (No asignados) en base a un termino de busqueda
   * @param query Termino de busqueda a aplicar
   */
  public searchOnlyAvailable(idUsuarioContratante: number, query: Observable<string>): Observable<Rastreable[]> {
    return query
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => this.getOnlyAvailableRastreables(idUsuarioContratante, term));
  }
  /**
   * devuelve una lista de Rastreables no asignados
   * @param query Termino de busqueda
   * @param idUsuarioContratante Usuario al que pertenece el Rastreable
   */
  public getOnlyAvailableRastreables(idUsuarioContratante: number, query?: string): Observable<Rastreable[]> {
    let params = new HttpParams();

    params = params.set('idUsuarioContratante', idUsuarioContratante + '');
    if (query !== undefined && query != null) {
      params = params.set('query', query + '');
    }

    return this.http.get(
      (this.URL_API + '/noasignados'),
      { params }
    )
    .map(this.fnOnGetRastreablesSucccess.bind(this))
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }
  public getRastreables(query?: string, idUsuarioContratante?: number): Observable<Rastreable[]> {
    let params = new HttpParams();
    if (query !== undefined && query != null) {
      params = params.set('query', query + '');
    }
    if (idUsuarioContratante !== undefined && idUsuarioContratante != null) {
      params = params.set('idUsuarioContratante', idUsuarioContratante + '');
    }
    return this.http.get(
      (this.URL_API),
      { params }
    )
    .map(this.fnOnGetRastreablesSucccess.bind(this))
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }
  public getRastreable(id: number | string): Observable<Rastreable> {
    const params = new HttpParams()
      .set('id', id + '');
    return this.http.get(
        (this.URL_API),
        {params: params}
    )
    .map(this.fnOnGetRastreableSucccess.bind(this));
  }
  public saveRastreable(objRastreable: Rastreable): Observable<number | any> {
    return (objRastreable.id != null) ? this.updateRastreable(objRastreable) : this.addRastreable(objRastreable);
  }
  // Retorna el id del Rastreable
  private addRastreable(objRastreable: Rastreable): Observable<number | any> {
    return this.http.post(this.URL_API, {objRastreable: objRastreable})
      .map((response: Rastreable) => {
        return response.id;
      });
  }
  private updateRastreable(objRastreable: Rastreable): Observable<number | any> {
    return this.http.put(this.URL_API, {objRastreable: objRastreable})
      .map((response: Rastreable) => {
        return response.id;
      });
  }
  private fnOnGetRastreablesSucccess(response): Rastreable[] {
    return response.map((objRastreable: Rastreable) => {
      return objRastreable;
    });
  }
  private fnOnGetRastreableSucccess(response): Rastreable {
    return <Rastreable> response;
  }
}
