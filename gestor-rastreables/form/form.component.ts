import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators, ReactiveFormsModule, FormBuilder } from '@angular/forms';

import { Rastreable } from '../_models/rastreable';
import { RastreableService } from '../_services/rastreable.service';
import { Usuario } from '../../gestor-usuarios/_models/usuario';
import { UsuarioService } from '../../gestor-usuarios/_services/usuario.service';

import { Globals } from '../../globals';
import { IValidacionConfigElemento, Escenarios } from '../../_models/config-validaciones';
import { Estatus } from '../../_models/estatus';
import { EstatusService } from '../../_services/estatus.service';
import { Subject } from 'rxjs/Subject';
import { Dispositivo } from '../../gestor-dispositivos/_models/dispositivo';
import { Subscription } from 'rxjs/Subscription';
import { DispositivoService } from '../../gestor-dispositivos/_services/dispositivo.service';
import { SearchSelectComponent } from '../../search-select/search-select.component';
import { MessagesSystemComponent } from '../../messages-system/messages-system.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private frmRastreable: FormGroup;
  private configsValidacion: Array<IValidacionConfigElemento> = [];
  /**
   * variables para el control de bandera de isLoading
   */
  private isLoading = false;
  private textIsLoading = 'Cargando...';

  private objRastreable: Rastreable;
  private objUsuarioPropietario: Usuario;
  private estatus: Observable<Estatus[]>;
  private objGlobals: Globals;

  private tiposSanguineos: Array<String> = ['A+', 'B+', 'AB+', 'O+', 'A-', 'B-', 'AB-', 'O-'];

  /**
   * Para el autocompletado de Dispositivos
   * Ya que los Dispositivos dependen del usuario contratante
   * la funcion de busqueda se le indicara al componente hasta
   * que se haya seleccionado un usuario contratante
   */
  @ViewChild('ssDispositivo')
  private searchSelectDispositivos: SearchSelectComponent;

  constructor(
    public route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private objRastreableService: RastreableService,
    private objUsuarioService: UsuarioService,
    private objDispositivoService: DispositivoService,
    private objEstatusService: EstatusService,
    private objMessagesSystem: MessagesSystemComponent
  ) {
    this.objRastreable = new Rastreable();
    this.objGlobals = new Globals();
    this.tiposSanguineos.sort();
    this.setConfigValidacion();
  }

  private setConfigValidacion() {
    this.configsValidacion = [
      {
        nombre: 'nombreCompleto',
        referencia: this.objRastreable.nombreCompleto,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: this.objGlobals.getValidatorForNames()
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(4)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(100)
          }
        ]
      },
      {
        nombre: 'email',
        referencia: this.objRastreable.email,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.email
          }
        ]
      },
      {
        nombre: 'telefono',
        referencia: this.objRastreable.telefono,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.minLength(7)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(10)
          },
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: this.objGlobals.getValidatorFortelephoneNumber()
          }
        ]
      },
      {
        nombre: 'imei',
        referencia: this.objRastreable.objDispositivo.imei,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(100)
          }
        ]
      },
      {
        nombre: 'descripcion',
        referencia: this.objRastreable.descripcion,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(100)
          }
        ]
      },
      {
        nombre: 'tipoSanguineo',
        referencia: this.objRastreable.tipoSanguineo,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.maxLength(3),
          }
        ]
      },
      {
        nombre: 'idEstatus',
        referencia: this.objRastreable.idEstatus,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required,
          }
        ]
      },
      {
        nombre: 'idUsuarioContratante',
        referencia: this.objRastreable.idUsuarioContratante,
        reglas: [
          {
            escenario: [Escenarios.INSERT, Escenarios.UPDATE],
            validacion: Validators.required
          }
        ]
      }
    ];
  }
  ngOnInit() {
    this.frmRastreable = this.objGlobals.preConfigurarValidadores(this.configsValidacion);
    this.getRastreablesFromQueryParams();

    this.estatus = this.objEstatusService.getEstatus();
  }
  /**
   * Verifica la URL en busca del ID de un Rastreables, en caso de encontrar un id
   * solicita al API el Rastreable, lo carga en el formulario junto con las reglas
   * de validacion dependiendo del escenario
   */
  private getRastreablesFromQueryParams(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') === null || isNaN(+params.get('id'))) {
          this.objGlobals.posConfigurarValidadores(this.frmRastreable, this.configsValidacion, Escenarios.INSERT);
        } else {
          // Hay id en parametros por lo tanto edita
          this.isLoading = true;
          this.objRastreableService.getRastreable(+params.get('id')).subscribe((resp) => {
            this.setDataOnForm(resp);
            this.objGlobals.posConfigurarValidadores(this.frmRastreable, this.configsValidacion, Escenarios.UPDATE);
            this.isLoading = false;
          });
        }
        return Observable.empty();
      }).subscribe();
  }
  /**
   * Despues de obtener el Rastreable desde el servidor se encarga de
   * montarlo en el formulario
   * @param objFromServer Objeto en JSON obtenido desde del servidor
   */
  private setDataOnForm(objFromServer: any): void {
    this.objRastreable = <Rastreable>objFromServer;
    this.onSelectUsuarioContratante(this.objRastreable.objUsuarioContratante);
  }

  //#region seccion de eventos
  /**
   * Evento click sobre el boton guardar
   */
  private onClickBtnSave(): any {
    if (!this.frmRastreable.valid) {
      this.objMessagesSystem.openSnackBar('Formulario no valido');
      return false;
    }
    this.isLoading = true;
    this.textIsLoading = 'Guardando...';
    this.objRastreableService
      .saveRastreable(this.objRastreable)
      .subscribe(response => {
        this.isLoading = false;
        this.objMessagesSystem.openSnackBar('Guardado con éxito');
        this.router.navigate(['/rastreables']);
      });
  }

  /**
   * Evento click sobre la flecha "atras"
   */
  private onClickBtnAtras(): any {
    this.location.back();
  }
  //#endregion


  //#region Funciones para Autocompletados
  //#region Autocompletado de Usuarios Contratantes
  /**
   * Funcion que el Componente utiliza para realizar la busqueda
   * cada vez que se tipee algo sobre el
   * @param search Texto a buscar
   */
  private fnSearchUsuarios(search: Observable<string>): any {
    return this.objUsuarioService.search(search);
  }
  /**
   * Evento al momento de vincular un usuario a la asignacion
   * @param itemUsuarioSelect Usuario Contratante seleccionado
   */
  private onSelectUsuarioContratante(itemUsuarioSelect: Usuario): void {
    let setValue = null;
    if (itemUsuarioSelect !== undefined && itemUsuarioSelect !== null) {
      setValue = itemUsuarioSelect.id;
    }
    // Se notifica al
    this.frmRastreable.get('idUsuarioContratante').setValue(setValue);
    this.objRastreable.idUsuarioContratante = setValue;

    // Solo configurar nueva busqueda si se selecciono usuario
    if (setValue !== null) {
      this.searchSelectDispositivos.setFnSearch(this.fnSearchDispositivos.bind(this));
    }
  }
  /**
   * Funcion que el Componente utiliza para obtener el texto
   * que sera visible del objeto seleccionado
   * @param objUsuarioItem Cada elemento a ser renderizado tanto en la lista
   * de sugerencias como al momento de haber seleccionado algun
   */
  private getTextObjUsuarioItem(objUsuarioItem: Usuario): string {
    if (objUsuarioItem !== undefined && objUsuarioItem != null) {
      return objUsuarioItem.datosPersonales.nombreCompleto;
    }
    return '';
  }
  //#endregion
  //#region Autocompletado de Dispositivos
  /**
   * Funcion que el Componente utiliza para realizar la busqueda
   * cada vez que se tipee algo sobre el
   * @param search Texto a buscar
   */
  private fnSearchDispositivos(search: Observable<string>): any {
    return this.objDispositivoService
      .searchOnlyDispSinAsignarRastreableByUser(
      this.objRastreable.idUsuarioContratante,
      search
      );
  }
  /**
   * Evento al momento de vincular un Dispositivo al Rastreable
   * @param itemDispositivoSelect Dispositivo seleccionado
   */
  private onSelectDispositivo(itemDispositivoSelect: Dispositivo): void {
    let setValue = null;
    if (itemDispositivoSelect !== undefined && itemDispositivoSelect !== null) {
      setValue = itemDispositivoSelect.id;
    }
    // Se notifica al
    this.objRastreable.idDispositivo = setValue;
  }
  /**
   * Funcion que el Componente utiliza para obtener el texto
   * que sera visible del objeto seleccionado
   * @param objDispositivoItem Cada elemento a ser renderizado tanto en la lista
   * de sugerencias como al momento de haber seleccionado algun
   */
  private getTextObjDispositivoItem(objDispositivoItem: Dispositivo): string {
    if (objDispositivoItem !== undefined && objDispositivoItem != null) {
      return objDispositivoItem.imei;
    }
    return '';
  }
  //#endregion
  //#endregion
  //#region Funciones varias
  /**
    * Dependiendo de la validacion aplicada y del tipo de error
    * generado devuelve un mensaje de error configurado en Globals
    * @param elemento Elemento del formulario
    */
  private getErrorMessage(elemento: any): string {
    return this.objGlobals.getErrorMessage(this.frmRastreable, elemento);
  }
  //#endregion
}
