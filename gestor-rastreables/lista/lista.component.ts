import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

import { Router, ActivatedRoute } from '@angular/router';

import { Rastreable } from '../_models/rastreable';
import { RastreableService } from '../_services/rastreable.service';

import { ToolbarService } from '../../toolbar/_services/toolbar.service';
import { LoginService } from '../../login/_services/login.service';
import { PermisosMap } from '../../_models/permisos-map';
import { LoginInterceptor } from '../../login/_services/login-interceptor';

@Component({
  selector: 'app-gestor-rastreables-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit, AfterViewInit, OnDestroy {
  private rastreables: Observable<Rastreable[]>;
  /**
   * variables para el control de bandera de isLoading
   */
  private isLoading = true;
  private subscriptionBandLoading: Subscription;

  // Bandera para indicar que la lista esta vacia
  private isEmpty = false;

  public searchTerm: Subject<string> = new Subject<string>();
  public subscripcionChangeSearch: Subscription = null;

  constructor(
    private service: RastreableService,
    private router: Router,
    private tbs: ToolbarService,
    private objLoginService: LoginService,
    private objInterceptor: LoginInterceptor
  ) {
    this.inicializarToolbarSearch();
  }
  /**
   * Hace visible el campo de busqueda del toolbar y Subscribe esta lista al
   * evento keyPressed del campo de busqueda del toolbar
   */
  private inicializarToolbarSearch(): void {
    // Notificar al toolbar que muestre el campo de busqueda
    this.tbs.fireBandIsVisibleFieldSearch(true);
    // Subscribir al evento change del campo de busqueda de la barra
    this.subscripcionChangeSearch = this.tbs.getBandKeyPressOnTxtSearch().subscribe(this.onSearchFieldChange.bind(this));
    // Inicializar la busqueda
    this.service.search(this.searchTerm).subscribe((resp: Array<Rastreable>) => {
      if (resp.length === 0) {
        this.isEmpty = true;
      } else {
        this.isEmpty = false;
      }
      this.rastreables = Observable.of(resp);
    });
  }

  ngOnInit() {
    // Se muestran todos
    this.searchTerm.next();
  }

  ngAfterViewInit() {
    /**
     * Suscribir a las notificaciones del interceptor de comunicaciones
     * para la bandera de isLoading
     */
    this.subscriptionBandLoading = this.objInterceptor
      .subscriptionBandLoading().subscribe(this.fnConstrolIsLoading.bind(this));
  }

  ngOnDestroy() {
    this.tbs.fireBandIsVisibleFieldSearch(false);
    this.subscripcionChangeSearch.unsubscribe();
  }

  /**
     * Click sobre el float action buttom de la lista de Rastreables
     */
  public onClickAddRastreable(): void {
    this.router.navigate(['/rastreables/add']);
  }
  /**
   * Click sobre un Rastreable del listado
   * @param itemRastreableSelect Rastreable seleccionado
   */
  public onClickListItem(itemRastreableSelect: Rastreable): void | boolean {
    if (!this.checkAccess(PermisosMap.RASTREABLES_EDITAR)) {
      return false;
    }
    this.router.navigate(['/rastreables', itemRastreableSelect.id]);
  }
  private checkAccessfbOnList(): boolean {
    return this.checkAccess(PermisosMap.RASTREABLES_CREAR);
  }
  /**
   * Verifica si el usuario en session cuenta con el permiso especificado
   * @param idPermiso Id del permiso a verificar
   */
  public checkAccess(idPermiso: number): boolean {
    return this.objLoginService.checkAccess(idPermiso);
  }
  /**
   * Funcion subscrita al servicio de toolbarrservice para realizar el filtrado
   * de la lista de Rastreables en base a lo que se escriba en el
   * campo de busqueda ubicado en el Rastreable toolbar.
   * @param query Texto a filtrar en el listado de Rastreables
   */
  public onSearchFieldChange(query: string): void {
    this.searchTerm.next(query);
  }
  /**
   * Controla la bandera de estatus del loading del interceptor de
   * comunicaciones
   * @param loading Bandera que indica si se esta cargando un recurso
   */
  private fnConstrolIsLoading (loading: boolean): void {
    if (this.isLoading !== loading) {
      this.isLoading = loading;
    }
  }
}

