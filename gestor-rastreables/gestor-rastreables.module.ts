import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule,  ReactiveFormsModule } from '@angular/forms';

import { GestorRastreablesRoutingModule } from './gestor-rastreables-routing.module';
import { MaterialModule } from '../material.module';

import { RastreableService } from './_services/rastreable.service';

import { ListaComponent } from './lista/lista.component';
import { FormComponent } from './form/form.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GestorRastreablesRoutingModule,
    MaterialModule
  ],
  declarations: [ListaComponent, FormComponent, HomeComponent],
  providers: [ RastreableService ]
})
export class GestorRastreablesModule { }
