import { Usuario } from '../../gestor-usuarios/_models/usuario';
import { Dispositivo } from '../../gestor-dispositivos/_models/dispositivo';

export class Rastreable {
    constructor(
        public id: number = null,
        public idUsuarioContratante: number = null,
        public objUsuarioContratante: Usuario = new Usuario(),
        public idDispositivo: number = null,
        public objDispositivo: Dispositivo = new Dispositivo(),
        public nombreCompleto: string = null,
        public tipoSanguineo: string = null,
        public descripcion: string = null,
        public telefono: string = null,
        public email: string = null,
        public idEstatus: number = 1,
        public estatus: string = null,
    ) {}
}
