import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestorUsuariosHomeComponent } from './gestor-usuarios-home.component';

describe('GestorUsuariosHomeComponent', () => {
  let component: GestorUsuariosHomeComponent;
  let fixture: ComponentFixture<GestorUsuariosHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestorUsuariosHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestorUsuariosHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
