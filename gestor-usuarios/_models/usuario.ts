import { Persona } from '../../_models/persona';
import { ElementoAuth } from '../../rbac/_models/elemento-auth';
import { TimeZone } from '../../_models/time-zone';

export class Usuario {
    constructor(
        public id: number = null,
        public idPersona: number = null,
        public username: string = null,
        public password: string = null,
        public image: string = null,
        public tz: string = null,
        public objTz: TimeZone = new TimeZone(),
        public idEstatus = 1,
        public estatus: string = null,
        public datosPersonales: Persona = new Persona(),
        public permisos: ElementoAuth[] = null
    ) {
    }
    /**
     * Regresa el nombre completo del usuario
     */
    public getFullName(): string {
        let nombreCompleto = this.datosPersonales.apPaterno + ' ';
        if (this.datosPersonales.apMaterno !== '' && this.datosPersonales.apMaterno.length > 0) {
            nombreCompleto += this.datosPersonales.apMaterno + ' ';
        }
        if (this.datosPersonales.nombre !== '' && this.datosPersonales.nombre.length > 0) {
            nombreCompleto += this.datosPersonales.nombre;
        }
        return nombreCompleto;
    }
}
